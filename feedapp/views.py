from django.shortcuts import render
from .models import FeedItem, RegisteredUser, User
from django.db.models import Q


def feed_list(request):
    """
    Render feeds as per filter:-
    * My posts.
    * Me and the posts of everyone I’m tracking.
    * Everybody’s posts
    """
    if request.POST:
        user = request.user
        if user.is_authenticated:
            try:
                option = request.POST.get('dropdown')
                feedback=FeedItem.objects.none()
                if option == '1':
                    feeds = FeedItem.objects.filter(user_id=user.id)
                if option == '2':
                    followers = RegisteredUser.objects.get(user_id =user.id).tracking.all().values_list('user__id',flat=True)
                    feeds = FeedItem.objects.filter(Q(user_id=user.id) | Q(user_id__in = followers)).distinct()
                if option == '3':
                    feeds = FeedItem.objects.all()
                return render(request,'feedapp/feed_list.html',{'feeds':feeds, "option":option})
            except RegisteredUser.DoesNotExist:
                feeds = FeedItem.objects.filter(user_id=user.id)
                return render(request,'feedapp/feed_list.html',{'feeds':feeds, "option":option})
        else:
            return render(request,'feedapp/feed_list.html',{'error':'Sorry you are not logged-in, Please login to see Feeds.'})

    return render(request,'feedapp/feed_list.html')
