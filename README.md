# ActivityFeed

## Installation and Working structure: 

- Create a virtualenv with python3 as default python, activate it and run the following commands in terminal

- Go to project directory using terminal.

- pip install -r requirements.txt

- `python manage.py runserver localhost:8000`  #run main django server

- `python manage.py createsuperuser` #run this to create superuser.

- Go to `localhost:8000/admin` on your browser.

- Login with user which you have created recently.

- Go to `localhost:8000` on your browser(now you can filter your feeds.)
